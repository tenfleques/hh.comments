// JavaScript Document
function commentBox(msgObj){
	//creates a comments box
	var bg = "bg-dark";
	var txtclass="text-dark";
	if(arguments[1]){
		bg = "bg-success";
		txtclass="text-success";
	}
	var html ='<div class="card border-0 text-center col-md-4 mb-3 pb-5">';
	html += '<div class="card-header border-0 '+bg+'">';
	html +=	'<h6 class="text-white">'+msgObj.author+'</h6>';
	html += '</div>';
	html += '<div class="card-body p-3">';
	html += '<div class="m-3 comment-bg '+bg+' opacity-3"></div>';
	html += '<b class="card-title '+txtclass+' opacity-1">'+msgObj.email+'</b>';
	html += '<p class="card-text text-dark">'+msgObj.comment+'</p>';
	html += '</div>';
	html += '</div>';
	return html;
}

$(function(){
	updateComments(); //populate the comments section
	var form = $('#needs-validation');
    form.on('submit', function(event) {
	  event.preventDefault();
      event.stopPropagation();
      if (form[0].checkValidity() === true) {
		var req = $.ajax({
		  url: "api/insertComment/",
		  method: "POST",
		  data: $(this).serialize(),
		  dataType: "json"
		})
		req.fail(function(e){
			console.log(e)
		});
		req.done(function(){
			form.find("input, textarea").val(""); // clears contents
			form.removeClass('was-validated'); // make sure after clearing form contents user do not get invalidity errors
		})
		req.always(function(){
			updateComments();
		});        
      }else
	      form.addClass('was-validated');
    });
	
	function updateComments(){
		var req = $.ajax({
		  url: "api/getComments/",
		  method: "POST",
		  data: {"token": "public"},
		  dataType: "json"
		})
		req.fail(function(e){
			console.log(e)
		});
		req.done(function(data){
			var html = "";
			for(var i in data){
				html += commentBox(data[i],i%2);
			}
			$(".comments").html(html);
		})
	}
	
})