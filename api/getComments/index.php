<?php
	include_once("../../bin/functions.php");
	$comments = array();
	$req = queryMysql("select username as author, email, commentmsg as comment from users join comments on users.userid=comments.author order by comments.commentid desc");
	while ($res = mysqli_fetch_assoc($req["result"]))
		$comments[] = array_map(function($var){
			return htmlSanity($var);
		},$res);
	
	header('Content-Type: application/json; charset=utf-8');
	echo json_encode($comments,JSON_UNESCAPED_UNICODE,JSON_NUMERIC_CHECK);
?>