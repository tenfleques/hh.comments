#assumption the initiator has and admin account in mysql on localhost
CREATE USER 'tendai_network'@'localhost' IDENTIFIED BY '%A9]+bmky8^%#@%*(923)53';
CREATE DATABASE IF NOT EXISTS tendai_network;
use tendai_network;
CREATE TABLE IF NOT EXISTS users (userid INT AUTO_INCREMENT PRIMARY KEY, username VARCHAR(256) not null, email VARCHAR(256) not null, unique(email))ENGINE InnoDB;
DESCRIBE users;
CREATE TABLE IF NOT EXISTS comments (commentid INT AUTO_INCREMENT PRIMARY KEY, author int unsigned not null, commentmsg VARCHAR(5000) not null, date date not null)ENGINE InnoDB;
DESCRIBE comments;
GRANT INSERT,SELECT,UPDATE ON tendai_network.* TO `tendai_network`@`localhost`;